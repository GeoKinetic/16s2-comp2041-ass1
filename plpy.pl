#!/usr/bin/perl -w

# written by andrewt@cse.unsw.edu.au September 2016
# as a starting point for COMP2041/9041 assignment 
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/plpy/
# Replace this comment with your own header comment

#$equality_operator_function = 0;

#high default number that will be replaced by the number of indents the input program uses
$indent_number = 10;

#flag for the existence of <=>
$equality_operator_present = 0;

#flag for the existence of ARGV
$sys_present = 0;

#flag for the existence of str.join()
$join_present = 0;

my @stdin;
while ($string = <>) {
    push @stdin, $string;

    #determines whether the indention is tabs or spaces, and if it's spaces, how many
    if ($string =~ /^(\t+)/) {
        $count = 4*length($1);
        #print $count;
    } else {
        $string =~ /^(\s{2,})/;
        $count = length($1);
        #print $count;
    }
    if (defined $count && $count < $indent_number && $count != 0) {
        $indent_number = $count;
    } else {
        $indent_number = 4;
    }

    #sets flag to print function if <=> is present in program
    if ($string =~ /<=>/) {
        $equality_operator_present = 1;
    }

    #sets flag to print function if @/$ARGV or <> or <STDIN> are present in the program
    if ($string =~ /(@|\$)ARGV/ || $string =~ /<(STDIN)*>/) {
        $sys_present = 1;
    }

    #sets flag to print function is str.join() is present in the program
    if ($string =~ /join\(.+\)/) {
        $join_present = 1;
    }
}

$x = 0;
foreach $line (@stdin) {
    #print $line;

    #series of checks for things that can occur in different types of lines
    if ($line =~ /^[^#]/) {
        #converts (x..y) to range(x,y)
        if ($line =~ /\((\$|\w)\w*\.\.(\$|\w)\w*\)/) {
            $line =~ s/\(\$(\w+)\.\.\$(\w+)\)/"range\(".$1.", ".($2+1)."\)"/ge;
            $line =~ s/\((\d+)\.\.(\d+)\)/"range\(".$1.", ".($2+1)."\)"/ge;
        }

        if ($line =~ /\s*join\(\s*(\"|\').*(\"|\')\s*,\s*(.*)\)/) {
            $line =~ s/\s*join\(\s*(\"|\')(.*)(\"|\')\s*,\s*(.*)\)/join_function('$2',$4)/;
            #print $2, $4, "\n";
        }

        if ($line =~ /chomp\s*(.+)/) {
            $line =~ s/chomp\s*(.+)/$1 = $1\.rstrip\('\\n'\)/g;
        }

        if ($line =~ /\s*split\s*\({0,1}\s*\/(.+)\/\s*,\s*(\w+)(\s*,\s*\d+\s*)*/) {
            $line =~ s/\s*split\s*\({0,1}\s*\/(.+)\/\s*,\s*(\w+)(\s*,\s*\d+\s*)*/$2\.split\(\"$1\"$3\)/;
        }
        
        if ($line =~ /\s*split\s*\({0,1}\s*\/\/\s*,\s*(\w+)\s*\)/) {
            $line =~ s/\s*split\s*\({0,1}\s*\/\/\s*,\s*(\w+)\s*\)/list\($1\)/;
        }

        #converts $ARGV[]
        if ($line =~ /\$ARGV\[\d+\]/) {
            $line =~ s/ARGV\[(\d+)\]/"sys.argv\[".($1+1)."\]"/e;
        }

        #converts text operators
        if ($line =~ /(\$|@|)\w+\s*(lt|gt|le|ge|eq|ne|cmp)\s*(\$|@|)\w+/) {
            $line =~ s/(\$\w+\s*)lt(\s*\$\w+)/$1<$2/;
            $line =~ s/(\$\w+\s*)gt(\s*\$\w+)/$1>$2/;
            $line =~ s/(\$\w+\s*)le(\s*\$\w+)/$1<=$2/;
            $line =~ s/(\$\w+\s*)ge(\s*\$\w+)/$1>=$2/;
            $line =~ s/((\$|@|.*)\w+\s*)eq(\s*(\$|@|.*)\w+)/$1==$3/;
            $line =~ s/(\$\w+\s*)ne(\s*\$\w+)/$1!=$2/;
            $line =~ s/(\$\w+\s*)cmp(\s*\$\w+)/$1<=>$2/;
        }

        #converts <=>
        if ($line =~ /<=>/) {
            #$line =~ s/(\$\w+)(\s*)<=>(\s*)(\$\w+)/equality_operator_function\($1, $4\)/g;
            $line = equality_operator_print($line);
        }

        #converts scalar to len
        if ($line =~ /scalar\s*\({0,1}\@(\w+)\){0,1}/) {
            $line =~ s/scalar\s*\({0,1}(\@\w+)\){0,1}/len\($1\)/;
        }

        #converts @ARGV
        if ($line =~ /\@ARGV/) {
            $line =~ s/\@ARGV/\@sys.argv\[1:\]/g;
        }

        #converts <STDIN>
        if ($line =~ /<STDIN>/) {
            $line =~ s/<STDIN>/sys.stdin.readline()/;
            $line =~ s/\[\](\s*)$/\(\)$1/;
        }

    }

    if ($line =~ /^#!/) {
    
        # translate #! line 
        
        print "#!/usr/local/bin/python3.5 -u\n";
        if ($sys_present == 1) {
            print "import sys\n";
        }
        if ($equality_operator_present == 1) {
            equality_operator_function_print();
            $equality_operator_present = 0;
        }
        if ($join_present == 1) {
            join_function_print();
            $join_present = 0;
        }
    } elsif ($line =~ /^\s*#/ || $line =~ /^\s*$/) {
        
        # Blank & comment lines can be passed unchanged
        
        print "$line\n";
    } elsif ($line =~ /^\s*print\s*("|\$|\w+)(.*)(\\n|)"[\s;]*$/) {
        # Python's print adds a new-line character by default
        # so we need to delete it from the Perl print statement

        $line = count_indent($line);
        $line =~ s/^\s*print\s*(.*)$/print \($1\)/g;
        $line =~ s/,\s*"\\n"\)$/\)\n/;
        $line =~ s/(\".+)\\n\"\)$/$1\"\)/;
        $line =~ s/\"(\w+\s*(\(|\s*)(@|\$)(.+)(\)|))\"/$1/g;
        $line =~ s/[^(\\n)]"\s*$/",\n/;
        $line =~ s/\s*" "\s*,//;

        if ($line =~ /(\$|@)/) {
            #print $line, "\n";
            $line =~ s/"$//;
            $line =~ s/",$//;
            $line =~ s/"\$(\w+)\"/$1/g;
            $line =~ s/"@//g;
            $line = variable($line);

            if ($line =~ /<=>/) {
                $line = equality_operator_print($line);
            }
            print "$line";
        } else {
            $line = variable($line);
            print "$line";
        }
    } elsif ($line =~ /(if|while|elsif|else)\s*\(/) {
        #print "yay\n";
        $line = count_indent($line);
        $line = variable($line);
        $line =~ s/^\s*if\s*\(/if /g;
        $line =~ s/^\s*while\s*\(/while /g;
        $line =~ s/^\s*elsif\s*\(/elif /g;
        $line =~ s/^\s*else\s*\(/else /g;
        $line =~ s/\)\s*\{/:/g;
        print "$line";
    } elsif ($line =~ /(for|foreach)\s*/) {
        $line = count_indent($line);
        $line =~ s/^\s*for\s*\(\$(\w+)\s*=\s*(\d+);\s*\$(\1)\s*(<|>)\s*(\d+)\s*;\s*\$(\1)(\+\+|--)\s*\)/"for ".$1." in range(".$2.", ".($5+1).") "/ge;
        #^eg: for ($i = 0; $i < 10; $i++) --> for i in range(0, 10)
        $line =~ s/^\s*(for|foreach)\s*\$(\w+)\s*(range\(.+,\s*.+\))/for $2 in $3 /g;
        $line =~ s/^\s*(for|foreach)\s*\$(\w+)\s*\((\d+)\.\.(\d+)\)/"for ".$1." in range(".$2.", ".($3+1).") "/ge;
        #^eg: for|foreach $i (0..9) --> for i in range(0, 9)
        $line =~ s/^\s*foreach\s*\$(\w+)\s*\(\s*(.+)\s*\)/for $1 in $2/g;
        #print "#!$line!";

        $line =~ s/\s*\{/:/g;
        $line = variable($line);
        print $line;
    } elsif ($line =~ /\s*(last|next|exit)\s*.*;/) {
        $line = count_indent($line);
        $line =~ s/^\s*last\s*(.*)\s*/break/;
        $line =~ s/^\s*next\s*(.*)\s*/continue/;
        $line =~ s/^\s*exit\s*(.*)\s*/sys\.exit()/;
        $line = variable($line);
        print $line, "\n";
    } elsif ($line =~ /\s*push\s+@\w+\s*,\s*.+;/) {
        $line = count_indent($line);
        $line =~ s/\s*push\s+@(\w+)\s*,\s*(.+)\s*/$1\.append($2)/;
        $line = variable($line);
        print $line;
    } elsif ($line =~ /^\s*(\$|@)\w+\s*.*;$/) {
        $line = count_indent($line);
        $line =~ s/(@.*)\(/$1\[/;
        $line =~ s/(@.*)\)/$1\]/;
        $line = variable($line);
        #$line =~ s/(<<|>>)/$1=/;
        print "$line";
    } elsif ($line =~ /(chomp|split|join|exit)\s*.+;/) {
        $line = count_indent($line);
        $line = variable($line);
        $line =~ s/chomp\s*(.+)/$1 = $1\.rstrip\('\\n'\)/g;
        $line =~ s/\s*split\s*\({0,1}\s*\/(.+)\/\s*,\s*(\w+)(\s*,\s*\d+\s*)*/$2\.split\(\"$1\"$3\)/;
        $line =~ s/\s*split\s*\({0,1}\s*\/\/\s*,\s*(\w+)\s*\)/list\($1\)/;
        $line =~ s/\s*join\(\s*(\".*\")\s*,\s*(.*)\)/join_function($1,\[$2\])/;
        print $line;
    } elsif ($line =~ /^\}$/ || $line =~ /^\{$/) {
        #$line =~ s/\}//;
    } else {
        # Lines we can't translate are turned into comments
        
        print "#$line\n";
    }

    $x++;
}

if ($indent_number == 1000 || $indent_number == 0) {
    $indent_number = 4;
}

#prints out indenting
sub count_indent {
    #$edit = $_[0];
    #$edit =~ s/^(\s*)./$1/;
    #print $edit;
    if ($_[0] =~ /^(\t+)/) {
        $count = 4*length($1);
        #print $count;
    } else {
        $_[0] =~ /^(\s*)/;
        $count = length($1);
        #print $count;
    }
    if ($count < $indent_number && $count != 0) {
        $indent_number = $count;
    }
    #print "$count";
    for (1..$count) { print " "; }
    $_[0] =~ s/^\s*//;
    $_[0] =~ s/;$//;
    #print "#", $_[0], "\n";
    return $_[0];
}

#removes $ and @ symbols on variables
sub variable {
    $_[0] =~ s/(?<=\\\\)(\$|@)(\w)/$1$3/g;
    $_[0] =~ s/([^\\])(\$|@)(\w)/$1$3/g;
    $_[0] =~ s/^(\$|@)//;
    $_[0] =~ s/;$//;
    return $_[0];
}

#converts <=> into function call
sub equality_operator_print {
    $_[0] =~ s/(\$\w+)(\s*)<=>(\s*)(\$\w+)/equality_operator_function\($1, $4\)/g;
    #print $_[0], "\n";
    #$equality_operator_function = 1;
    return $_[0];
}

#prints out <=> function
sub equality_operator_function_print {
    #python function to perform <=> operation
    print "\n\n#created by plpy.pl for <=> operation\n";
    print "def equality_operator_function (operator1, operator2):\n";
    for (1..$indent_number) { print " "; }
    print "if operator1 - operator2 == 0:\n";
    for (1..2*$indent_number) { print " "; }
    print "return 0\n";
    for (1..$indent_number) { print " "; }
    print "if operator1 - operator2 < 0:\n";
    for (1..2*$indent_number) { print " "; }
    print "return -1\n";
    for (1..$indent_number) { print " "; }
    print "if operator1 - operator2 > 0:\n";
    for (1..2*$indent_number) { print " "; }
    print "return 1\n";
}

#prints out join function
sub join_function_print {
    print "\n\n#created by plpy/pl for str.join() function\n";
    print "def join_function (delimiter, strings):\n";
    for (1..$indent_number) { print " "; }
    print "array = delimiter.join(strings)\n";
    for (1..$indent_number) { print " "; }
    print "return array\n";
}